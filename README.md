# Modlitewnik - Polityka Prywatności

Modlitewnik jest aplikacją bezpłatną.

Niniejsza strona powstała po to aby poinformować o sposobie, przetwarzaniu i przechowywaniu danych w tym danych osobowych.

Aplikacja nie jest przeznaczona do zbierania i przechowywania danych osobowych. Żadne dane osobowe nie powinny być wprowadzane do aplikacji.

Aplikacja używa sdk-ów i serwisów firm trzecich:

* Google play - https://policies.google.com/privacy
* Firebase (Firebase Authentication, Realtime Database) - https://firebase.google.com/support/privacy

## Zakładanie konta

Zakładając konto możesz użyć swojego aktualnego konta Googlw lub dowolnego innego konta email pod warunkiem, że nie stanowi ono danej osobowej.
W przeciwnym razie powinieneś kontynuwać używanie aplikacji w trybie "gość".

## Dane przechowywane na serwerze

Dane na serwerze są przechowywane tylko pod warunkiem zalogowania się do aplikacji poprzez konto Google lub adres email.

* adres email konta lub skojarzony z kontem Google,
* ustawienia aplikacji,
* dodane modlitwy,
* oznaczenie modlitw ulubionych,
* oznaczenie modlitw odmówionych,
* oznaczenie rachunku sumienia,
* definicje przypomnień.

Jeżeli nie chcesz aby na serwerze były przechowywane poniższe dane wybierz opcję: "Kontynuuj jako gość".

## Usuwanie danych przechowywanych na serwerze.

Aby usunąć dane przechowywane na serwerze wyślij żądanie na adres: Tomasz.Smutek.dev@gmail.com z adresu email, którego to żądanie dotyczy.
W tytule maila umieść: "Usuwanie danych przechowywanych na serwerze".
Konto powiązane z tymi danymi nie będzie usunięte. Aby usunąć konto i dane przechowywane na serwerze przejdź do sekcji: "Usuwanie konta"

## Usuwanie konta.

Aby usunąć konto na serwerze wyślij żądanie na adres: Tomasz.Smutek.dev@gmail.com z adresu email, którego to żądanie dotyczy.
W tytule maila umieść: "Usuwanie konta".
Przed usunięciem konta zostaną najpierw usunięte dane przechowywane na serwerze zwiazane z tym kontem.


## Postanowienia końcowe

Poprzeużywanie aplikacji Użytkownik akceptuje zasady zawarte w Polityce Prywatności.
Twórca aplikacji zastrzega sobie prawo wprowadzenia zmian w Polityce Prywatności, o czym poinformuje Użytkownika za pośrednictwem Aplikacji.
Jeżeli Użytkownik nie wyraża zgody na wprowadzone zmiany, zobowiązany jest trwale usunąć Aplikację ze swojego urządzenia mobilnego. 


Do obowiązków użytkownika należy w należyty sposób zabezpieczyć urządzenie na którym zainstalowana jest aplikacja przed możliwością odczytu danych w przypadku utraty urządzenia.
Twórca aplikacji z przyczyn od siebie niezależnych nie może zapewnić prawidłowej pracy aplikacji w każdych warunkach, w związku z powyższym nie ponosi żadnej odpowiedzialności cywilnoprawnej za skutki wadliwego działania aplikacji.
Użytkownik na własne ryzyko i własną odpowiedzialność korzysta z aplikacji. 
